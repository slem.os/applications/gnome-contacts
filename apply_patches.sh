#!/bin/bash

# Variables
name="gnome-contacts"
version="3.36.1.pinephone"
# Script
tar -xzvf ${name}-*.tar.gz
original_folder=$(ls -d ${name}-* | grep -v "tar.gz")
mv $original_folder ${name}-${version}
cd ${name}-${version}
for i in $(ls -d debian/patches/*.patch); do patch -p1 < $i; done
cd ..
tar -czvf ${name}-${version}.tar.gz ${name}-${version}
